Imagine
=====

Replacement of Glide.

Configuration
-------------
```
extensions:
    imagine: Slts\Imagine\DI\ImagineExtension
    
imagine:
    signer:
        secret: "FILLME"
    loaders:
        default:
            flysystem:
                filesystem_service: @flysystem.filesystem.default
    resolvers:
        default:
            flysystem:
                filesystem_service: @flysystem.filesystem.default
                root_url: %site.url%
                cache_prefix: media/cache
                visibility: public
    data_loader: default
    routes:
        filter: App:Imagine:filter
        filterRuntime: App:Imagine:filterRuntime
    driver: gd
    filter_sets:
        default: []
        event-list:
            filters:
                relative_resize:
                    widen: 520
        gallery-preview:
            filters:
                thumbnail:
                    size: [200, 100]
```

Router config example
```
$router[] = new Route(
    'imagine/<action>/<filter>/<path .+>',
    [
        'module' => 'App',
        'presenter' => 'Imagine',
    ]
);
```

Usage
```
{$path|imageurl:'default', ['thumbnail' => ['size' => [360,480]]]}
```
