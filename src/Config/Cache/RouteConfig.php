<?php

namespace Slts\Imagine\Config\Cache;

class RouteConfig
{
    private $filterRoute;
    private $filterRuntimeRoute;

    public function __construct(
        string $filterRoute,
        string $filterRuntimeRoute
    ) {
        $this->filterRoute = $filterRoute;
        $this->filterRuntimeRoute = $filterRuntimeRoute;
    }

    public function getFilterRoute(): string
    {
        return $this->filterRoute;
    }

    public function getFilterRuntimeRoute(): string
    {
        return $this->filterRuntimeRoute;
    }
}
