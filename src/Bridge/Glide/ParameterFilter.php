<?php

namespace Slts\Imagine\Bridge\Glide;

class ParameterFilter
{
    public static function filter(array $parameters)
    {
        $allowedParameters = Configuration::getParameters();

        return array_filter($parameters, function ($param, $key) use ($allowedParameters){
            return in_array($key, $allowedParameters);
        }, ARRAY_FILTER_USE_BOTH);
    }
}
