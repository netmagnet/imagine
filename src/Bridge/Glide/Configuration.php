<?php

namespace Slts\Imagine\Bridge\Glide;

class Configuration
{
    const ROUTING_PARAM_NAME = 'path';
    
    /**
     * @var array
     * @see http://glide.thephpleague.com/1.0/api/quick-reference/
     */
    private static $parameters = [
        'or', /* Orientation */
        'crop', /* Crop */
        'w', /* Width */
        'h', /* Height */
        'fit', /* Fit */
        'dpr', /* Device pixel ratio */
        'bri', /* Brightness */
        'con', /* Contrast */
        'gam', /* Gamma */
        'sharp', /* Sharpen */
        'blur', /* Blur */
        'pixel', /* Pixelate */
        'filt', /* Filter */
        'bg', /* Background */
        'border', /* Border */
        'q', /* Quality */
        'fm', /* Format */
        'p', /* presets */
        's', /* special param - protection signature */
        'mark', /* Watermark Path */
        'markw', /* Watermark Width */
        'markh', /* Watermark Height */
        'markx', /* Watermark X-offset */
        'marky', /* Watermark Y-offset */
        'markpad', /* Watermark Padding */
        'markpos', /* Watermark Position */
    ];

    public static function getParameters()
    {
        return self::$parameters;
    }
}
