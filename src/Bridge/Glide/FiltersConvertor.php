<?php

namespace Slts\Imagine\Bridge\Glide;

class FiltersConvertor
{
    public static function glideToImagine(array $glideParams): array
    {
        $result = [null, $glideParams];
        if (null !== $filter = $glideParams['p'] ?? null) {
            unset($glideParams['p']);
            $result = [
                $filter,
                $glideParams
            ];
        }

        return $result;
    }
}
