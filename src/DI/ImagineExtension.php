<?php

namespace Slts\Imagine\DI;

use Imagine\Image\ImagineInterface;
use Imagine\Image\Metadata\ExifMetadataReader;
use Imagine\Image\Metadata\MetadataReaderInterface;
use Liip\ImagineBundle\Binary\Loader\ChainLoader;
use Liip\ImagineBundle\Binary\Loader\FlysystemLoader;
use Liip\ImagineBundle\Binary\SimpleMimeTypeGuesser;
use Liip\ImagineBundle\Imagine\Cache\Signer;
use Liip\ImagineBundle\Imagine\Data\DataManager;
use Liip\ImagineBundle\Imagine\Filter\FilterConfiguration;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;
use Liip\ImagineBundle\Service\FilterService;
use Nette;
use Nette\Bridges\ApplicationLatte\ILatteFactory;
use Nette\DI\CompilerExtension;
use Nette\DI\ContainerBuilder;
use Nette\DI\Definitions\ServiceDefinition;
use Slts\Imagine\Cache\CacheManager;
use Slts\Imagine\Cache\Resolver\FlysystemResolver;
use Slts\Imagine\Cache\Resolver\WebPathResolver;
use Slts\Imagine\Config\Cache\RouteConfig;
use Slts\Imagine\Filter\Loader\BlurFilterLoader;
use Slts\Imagine\Filter\Loader\BrightnessFilterLoader;
use Slts\Imagine\Templating\Latte\Filters\ImagineFilter;
use Symfony\Component\Mime\MimeTypes;

class ImagineExtension extends CompilerExtension
{
    public function getConfigSchema(): Nette\Schema\Schema
    {
        return Nette\Schema\Expect::structure([
            'resolvers' => Nette\Schema\Expect::array([]),
            'loaders' => Nette\Schema\Expect::array([]),
            'signer' => Nette\Schema\Expect::structure([
                'secret' => Nette\Schema\Expect::string('CHANGEME'),
            ]),
            'data_loader' => Nette\Schema\Expect::string('default'),
            'routes' => Nette\Schema\Expect::structure([
                'filter' => Nette\Schema\Expect::string('App:Imagine:filter'),
                'filterRuntime' => Nette\Schema\Expect::string('App:Imagine:filterRuntime'),
            ]),
            'driver' => Nette\Schema\Expect::string('gd'),
            'filter_sets' => Nette\Schema\Expect::array([
                'default' => Nette\Schema\Expect::array([]),
            ]),
            'jpegoptim' => Nette\Schema\Expect::structure([
                'binary' => Nette\Schema\Expect::string('/usr/bin/jpegoptim'),
                'stripAll' => Nette\Schema\Expect::bool(true),
                'max' => Nette\Schema\Expect::int(null),
                'progressive' => Nette\Schema\Expect::bool(true),
                'tempDir' => Nette\Schema\Expect::string(null),
            ]),
            'optipng' => Nette\Schema\Expect::structure([
                'binary' => Nette\Schema\Expect::string('/usr/bin/optipng'),
                'level' => Nette\Schema\Expect::int(7),
                'stripAll' => Nette\Schema\Expect::bool(true),
                'tempDir' => Nette\Schema\Expect::string(null),
            ]),
            'pngquant' => Nette\Schema\Expect::structure([
                'binary' => Nette\Schema\Expect::string('/usr/bin/pngquant'),
            ]),
            'mozjpeg' => Nette\Schema\Expect::structure([
                'binary' => Nette\Schema\Expect::string('/opt/mozjpeg/bin/cjpeg'),
            ]),
            'paste_image' => Nette\Schema\Expect::structure([
                'dir' => Nette\Schema\Expect::string('%appDir%/../www'),
            ]),
            'paste' => Nette\Schema\Expect::structure([
                'dir' => Nette\Schema\Expect::string('%appDir%/../www'),
            ]),
            'watermark' => Nette\Schema\Expect::structure([
                'dir' => Nette\Schema\Expect::string('%appDir%/../www'),
            ]),
            'watermark_image' => Nette\Schema\Expect::structure([
                'dir' => Nette\Schema\Expect::string('%appDir%/../www'),
            ]),
            'cache' => Nette\Schema\Expect::string(null), //TODO implement
        ]);
    }

    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();
        $config = $this->getConfig();

        $fc = $builder
            ->addDefinition($this->prefix('filterConfiguration'))
            ->setFactory(FilterConfiguration::class, [$config->filter_sets])
        ;

        $builder
            ->addDefinition($this->prefix('signer'))
            ->setFactory(Signer::class, [$config->signer->secret])
        ;

        $builder
            ->addDefinition($this->prefix('routeConfig'))
            ->setFactory(RouteConfig::class, [
                $config->routes->filter ?? 'App:Imagine:filter',
                $config->routes->filterRuntime ?? 'App:Imagine:filterRuntime',
            ])
        ;

        $cm = $builder
            ->addDefinition($this->prefix('cacheManager'))
            ->setFactory(CacheManager::class)
        ;

        $builder
            ->addDefinition($this->prefix('filter'))
            ->setFactory(ImagineFilter::class)
        ;

        $builder
            ->addDefinition($this->prefix('meta_data.reader'))
            ->setFactory(ExifMetadataReader::class)
        ;

        // imagine
        $imagineClass = \Imagine\Gd\Imagine::class;
        if (isset($config->driver) && $config->driver === 'imagick') {
            $imagineClass = \Imagine\Imagick\Imagine::class;
        }
        if (isset($config->driver) && $config->driver === 'gmagick') {
            $imagineClass = \Imagine\Gmagick\Imagine::class;
        }
        $id = $builder
            ->addDefinition($this->prefix('imagine'))
            ->setType(ImagineInterface::class)
            ->setFactory($imagineClass)
            ->addSetup('?->setMetadataReader(?)', ['@self', $builder->getDefinitionByType(MetadataReaderInterface::class)])
        ;

        // guessers
        $mtgd = $builder
            ->addDefinition($this->prefix('mime_type_guesser'))
            ->setFactory(MimeTypes::class)
        ;
        $mtg = $builder
            ->addDefinition($this->prefix('binary.mime_type_guesser'))
            ->setFactory(SimpleMimeTypeGuesser::class, [$mtgd])
        ;

        $dm = $builder
            ->addDefinition($this->prefix('dataManager'))
            ->setFactory(DataManager::class, [
                $mtg,
                $mtgd,
                $fc,
                $config->data_loader
            ])
        ;

        $fm = $builder
            ->addDefinition($this->prefix('filterManager'))
            ->setFactory(FilterManager::class, [])
        ;

        $builder
            ->addDefinition($this->prefix('filterService'))
            ->setFactory(FilterService::class, [$dm, $fm, $cm, false, []])
        ;

        $this->registerLoaders($builder, $config, $mtgd);
        $this->registerResolvers($builder, $config);
        $this->registerFilterLoaders($builder, $id, $config);
        $this->registerPostProcessors($builder, $config);
    }

    public function beforeCompile()
    {
        $builder = $this->getContainerBuilder();

        $filterDef = $builder->getDefinition($this->prefix('filter'));
        $registerToLatte = function (Nette\DI\Definitions\Definition $def) use ($filterDef) {
            $def->getResultDefinition()->addSetup('?->addFilter(?, ?)', ['@self', 'imageurl', $filterDef]);
        };

        $latteFactoryService = $builder->getByType(ILatteFactory::class) ?: 'nette.latteFactory';
        if ($builder->hasDefinition($latteFactoryService)) {
            $registerToLatte($builder->getDefinition($latteFactoryService));
        }

        if ($builder->hasDefinition('nette.latte')) {
            $registerToLatte($builder->getDefinition('nette.latte'));
        }

        $this->fillCacheManager($builder);
        $this->fillDataManager($builder);
        $this->fillFilterManager($builder);
    }

    private function registerLoaders(ContainerBuilder $builder, \stdClass $config, ServiceDefinition $eg)
    {
        foreach ($config->loaders as $name => $resolver) {
            reset($resolver);
            $type = key($resolver);
            $resolverConf = current($resolver);

            if ($type === 'flysystem') {
                $args = [
                    $eg,
                    $resolverConf['filesystem_service'],
                ];

                $builder
                    ->addDefinition($this->prefix("loader.prototype.{$name}"))
                    ->setFactory(FlysystemLoader::class, $args)
                    ->addTag('liip_imagine.data.loader')
                    ->addTag('loader', $name)
                ;

                continue;
            }

            if ($type === 'chain') {
                $args = [];
                foreach ($resolverConf['loaders'] as $loaderName) {
                    $args[] = $builder->getDefinition($this->prefix("loader.prototype.{$loaderName}"));
                }

                $builder
                    ->addDefinition($this->prefix("loader.prototype.{$name}"))
                    ->setFactory(ChainLoader::class, [$args])
                    ->addTag('liip_imagine.data.loader')
                    ->addTag('loader', $name)
                ;
            }
        }
    }

    private function registerResolvers(ContainerBuilder $builder, \stdClass $config)
    {
        foreach ($config->resolvers as $name => $resolver) {
            reset($resolver);
            $type = key($resolver);
            $resolverConf = current($resolver);

            if ($type === 'flysystem') {
                $args = [];
                $args[] = $resolverConf['filesystem_service'];
                $args[] = $resolverConf['root_url'];
                if (isset($resolverConf['cache_prefix'])) {
                    $args[] = $resolverConf['cache_prefix'];
                }
                if (isset($resolverConf['visibility'])) {
                    $args[] = $resolverConf['visibility'];
                }

                $builder
                    ->addDefinition($this->prefix("resolver.{$name}"))
                    ->setFactory(FlysystemResolver::class, $args)
                    ->addTag('liip_imagine.cache.resolver')
                    ->addTag('resolver', $name)
                ;

                continue;
            }

            if ($type === 'web_path') {
                $args = [];
                $args[] = $builder->getDefinition('request');
                $args[] = $resolverConf['web_root_dir'];
                if (isset($resolverConf['cache_prefix'])) {
                    $args[] = $resolverConf['cache_prefix'];
                }

                $builder
                    ->addDefinition($this->prefix("resolver.{$name}"))
                    ->setFactory(WebPathResolver::class, $args)
                    ->addTag('liip_imagine.cache.resolver')
                    ->addTag('resolver', $name)
                ;
            }
        }
    }

    private function registerFilterLoaders(ContainerBuilder $builder, ServiceDefinition $id, \stdClass $config)
    {
        $builder
            ->addDefinition($this->prefix('filter.loader.relative_resize'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\RelativeResizeFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'relative_resize');

        $builder
            ->addDefinition($this->prefix('filter.loader.resize'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\ResizeFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'resize');

        $builder
            ->addDefinition($this->prefix('filter.loader.thumbnail'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\ThumbnailFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'thumbnail');

        $builder
            ->addDefinition($this->prefix('filter.loader.crop'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\CropFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'crop');

        $builder
            ->addDefinition($this->prefix('filter.loader.grayscale'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\GrayscaleFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'grayscale');

        $builder
            ->addDefinition($this->prefix('filter.loader.paste_image'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\PasteFilterLoader::class, [
                $id,
                $config->paste_image->dir,
            ])
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'paste_image');

        $builder
            ->addDefinition($this->prefix('filter.loader.paste'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\PasteFilterLoader::class, [
                $id,
                $config->paste->dir,
            ])
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'paste');

        $builder
            ->addDefinition($this->prefix('filter.loader.watermark'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\WatermarkFilterLoader::class, [
                $id,
                $config->watermark->dir,
            ])
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'watermark');

        $builder
            ->addDefinition($this->prefix('filter.loader.watermark_image'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\WatermarkFilterLoader::class, [
                $id,
                $config->watermark_image->dir,
            ])
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'watermark_image');

        $builder
            ->addDefinition($this->prefix('filter.loader.background'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\BackgroundFilterLoader::class, [
                $id,
            ])
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'background');

        $builder
            ->addDefinition($this->prefix('filter.loader.strip'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\StripFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'strip');

        $builder
            ->addDefinition($this->prefix('filter.loader.scale'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\ScaleFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'scale');

        $builder
            ->addDefinition($this->prefix('filter.loader.upscale'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\UpscaleFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'upscale');

        $builder
            ->addDefinition($this->prefix('filter.loader.downscale'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\DownscaleFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'downscale');

        $builder
            ->addDefinition($this->prefix('filter.loader.auto_rotate'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\AutoRotateFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'auto_rotate');

        $builder
            ->addDefinition($this->prefix('filter.loader.rotate'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\RotateFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'rotate');

        $builder
            ->addDefinition($this->prefix('filter.loader.flip'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\FlipFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'flip');

        $builder
            ->addDefinition($this->prefix('filter.loader.interlace'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\InterlaceFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'interlace');

        $builder
            ->addDefinition($this->prefix('filter.loader.resample'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\ResampleFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'resample');

        $builder
            ->addDefinition($this->prefix('filter.loader.fixed'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\Loader\FixedFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'fixed');

        $builder
            ->addDefinition($this->prefix('filter.loader.brightness'))
            ->setFactory(BrightnessFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'brightness');

        $builder
            ->addDefinition($this->prefix('filter.loader.blur'))
            ->setFactory(BlurFilterLoader::class)
            ->addTag('liip_imagine.filter.loader')
            ->addTag('loader', 'blur');
    }

    private function registerPostProcessors(ContainerBuilder $builder, \stdClass $config)
    {
        $builder
            ->addDefinition($this->prefix('filter.post_processor.jpegoptim'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\PostProcessor\JpegOptimPostProcessor::class, [
                $config->jpegoptim->binary,
                $config->jpegoptim->stripAll,
                $config->jpegoptim->max,
                $config->jpegoptim->progressive,
                $config->jpegoptim->tempDir,
            ])
            ->addTag('liip_imagine.filter.post_processor')
            ->addTag('post_processor', 'jpegoptim');
        $builder
            ->addDefinition($this->prefix('filter.post_processor.optipng'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\PostProcessor\OptiPngPostProcessor::class, [
                $config->optipng->binary,
                $config->optipng->level,
                $config->optipng->stripAll,
                $config->optipng->tempDir,
            ])
            ->addTag('liip_imagine.filter.post_processor')
            ->addTag('post_processor', 'optipng');
        $builder
            ->addDefinition($this->prefix('filter.post_processor.pngquant'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\PostProcessor\PngquantPostProcessor::class, [
                $config->pngquant->binary,
            ])
            ->addTag('liip_imagine.filter.post_processor')
            ->addTag('post_processor', 'pngquant');
        $builder
            ->addDefinition($this->prefix('filter.post_processor.mozjpeg'))
            ->setFactory(\Liip\ImagineBundle\Imagine\Filter\PostProcessor\MozJpegPostProcessor::class, [
                $config->mozjpeg->binary,
            ])
            ->addTag('liip_imagine.filter.post_processor')
            ->addTag('post_processor', 'mozjpeg');
    }

    private function fillCacheManager(ContainerBuilder $builder)
    {
        $def = $builder->getDefinition($this->prefix('cacheManager'));

        foreach ($builder->findByTag('liip_imagine.cache.resolver') as $defName => $whateverBool) {
            $loaderDef = $builder->getDefinition($defName);
            $name = $loaderDef->getTag('resolver');
            $def->addSetup('?->addResolver(?, ?)', ['@self', $name, $loaderDef]);
        }
    }

    private function fillDataManager(ContainerBuilder $builder)
    {
        $def = $builder->getDefinition($this->prefix('dataManager'));

        foreach ($builder->findByTag('liip_imagine.data.loader') as $defName => $whateverBool) {
            $loaderDef = $builder->getDefinition($defName);
            $name = $loaderDef->getTag('loader');
            $def->addSetup('?->addLoader(?, ?)', ['@self', $name, $loaderDef]);
        }
    }

    private function fillFilterManager(ContainerBuilder $builder)
    {
        $def = $builder->getDefinition($this->prefix('filterManager'));

        foreach ($builder->findByTag('liip_imagine.filter.loader') as $defName => $whateverBool) {
            $loaderDef = $builder->getDefinition($defName);
            $name = $loaderDef->getTag('loader');
            $def->addSetup('?->addLoader(?, ?)', ['@self', $name, $loaderDef]);
        }

        foreach ($builder->findByTag('liip_imagine.filter.post_processor') as $defName => $whateverBool) {
            $loaderDef = $builder->getDefinition($defName);
            $name = $loaderDef->getTag('post_processor');
            $def->addSetup('?->addPostProcessor(?, ?)', ['@self', $name, $loaderDef]);
        }
    }
}
