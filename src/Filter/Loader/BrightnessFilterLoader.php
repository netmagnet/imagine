<?php

namespace Slts\Imagine\Filter\Loader;

use Imagine\Image\ImageInterface;
use Liip\ImagineBundle\Imagine\Filter\Loader\LoaderInterface;

class BrightnessFilterLoader implements LoaderInterface
{
    public function load(ImageInterface $image, array $options = [])
    {
        $value = $options['value'] ?? 0;
        if (!is_numeric($value)) {
            return $image;
        }
        if ($value < -100 || $value > 100) {
            return $image;
        }

        $image
            ->effects()
            ->brightness($value)
        ;

        return $image;
    }
}
