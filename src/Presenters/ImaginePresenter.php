<?php

namespace Slts\Imagine\Presenters;

use Imagine\Exception\RuntimeException;
use Liip\ImagineBundle\Exception\Binary\Loader\NotLoadableException;
use Liip\ImagineBundle\Exception\Imagine\Filter\NonExistingFilterException;
use Liip\ImagineBundle\Imagine\Cache\Helper\PathHelper;
use Liip\ImagineBundle\Imagine\Cache\SignerInterface;
use Liip\ImagineBundle\Imagine\Data\DataManager;
use Liip\ImagineBundle\Service\FilterService;
use Nette\Application\BadRequestException;
use Nette\Application\Responses\RedirectResponse;
use Nette\Application\UI\Presenter;
use Nette\Http\IResponse;
use Nette\Http\Request;
use Slts\Imagine\Config\Presenter\PresenterConfig;

class ImaginePresenter extends Presenter
{
    /**
     * @var FilterService
     */
    private $filterService;

    /**
     * @var DataManager
     */
    private $dataManager;

    /**
     * @var SignerInterface
     */
    private $signer;

    /**
     * @var PresenterConfig
     */
    private $presenterConfig;

    /**
     * @param FilterService        $filterService
     * @param DataManager          $dataManager
     * @param SignerInterface      $signer
     * @param PresenterConfig|null $presenterConfig
     */
    public function __construct(
        FilterService $filterService,
        DataManager $dataManager,
        SignerInterface $signer,
        ?PresenterConfig $presenterConfig = null
    ) {
        parent::__construct();

        $this->filterService = $filterService;
        $this->dataManager = $dataManager;
        $this->signer = $signer;

        if (null === $presenterConfig) {
            @trigger_error(sprintf(
                'Instantiating "%s" without a forth argument of type "%s" is deprecated since 2.2.0 and will be required in 3.0.', self::class, PresenterConfig::class
            ), E_USER_DEPRECATED);
        }

        $this->presenterConfig = $presenterConfig ?? new PresenterConfig(301);
    }

    /**
     * This action applies a given filter to a given image, saves the image and redirects the browser to the stored
     * image.
     *
     * The resulting image is cached so subsequent requests will redirect to the cached image instead applying the
     * filter and storing the image again.
     */
    public function actionFilter()
    {
        $path = $this->getParameter('path', '');
        $path = PathHelper::urlPathToFilePath($path);
        $resolver = $this->getParameter('resolver', null);
        $filter = $this->getParameter('filter', '');

        $this->sendResponse(
            $this->createRedirectResponse(function () use ($path, $filter, $resolver) {
                return $this->filterService->getUrlOfFilteredImage(
                    $path,
                    $filter,
                    $resolver,
                    $this->isWebpSupported($this->getHttpRequest())
                );
            }, $path, $filter)
        );
    }

    /**
     * This action applies a given filter -merged with additional runtime filters- to a given image, saves the image and
     * redirects the browser to the stored image.
     *
     * The resulting image is cached so subsequent requests will redirect to the cached image instead applying the
     * filter and storing the image again.
     */
    public function actionFilterRuntime()
    {
        $path = $this->getParameter('path', '');
        $path = PathHelper::urlPathToFilePath($path);
        $resolver = $this->getParameter('resolver', 'default');
        $filter = $this->getParameter('filter', '');
        $hash = $this->getParameter('hash', '');
        $runtimeConfig = $this->getParameter('filters', []);

        if (!\is_array($runtimeConfig)) {
            $this->error(
                sprintf('Filters must be an array. Value was "%s"', $runtimeConfig),
                IResponse::S404_NOT_FOUND
            );
        }

        if (true !== $this->signer->check($hash, $path, $runtimeConfig)) {
            $this->error(
                sprintf(
                    'Signed url does not pass the sign check for path "%s" and filter "%s" and runtime config %s',
                    $path,
                    $filter,
                    json_encode($runtimeConfig)
                ),
                IResponse::S400_BAD_REQUEST
            );
        }

        $this->sendResponse(
            $this->createRedirectResponse(function () use ($path, $filter, $runtimeConfig, $resolver) {
                return $this->filterService->getUrlOfFilteredImageWithRuntimeFilters(
                    $path,
                    $filter,
                    $runtimeConfig,
                    $resolver,
                    $this->isWebpSupported($this->getHttpRequest())
                );
            }, $path, $filter, $hash)
        );
    }

    /**
     * @param \Closure    $url
     * @param string      $path
     * @param string      $filter
     * @param string|null $hash
     *
     * @return RedirectResponse
     */
    private function createRedirectResponse(\Closure $url, string $path, string $filter, ?string $hash = null): RedirectResponse
    {
        try {
            return new RedirectResponse($url(), $this->presenterConfig->getRedirectResponseCode());
        } catch (NotLoadableException $exception) {
            if (null !== $this->dataManager->getDefaultImageUrl($filter)) {
                return new RedirectResponse($this->dataManager->getDefaultImageUrl($filter));
            }

            throw new BadRequestException(
                sprintf('Source image for path "%s" could not be found', $path),
                IResponse::S404_NOT_FOUND,
                $exception
            );
        } catch (NonExistingFilterException $exception) {
            throw new BadRequestException(
                sprintf('Requested non-existing filter "%s"', $filter),
                IResponse::S404_NOT_FOUND,
                $exception
            );
        } catch (RuntimeException $exception) {
            throw new \RuntimeException(vsprintf('Unable to create image for path "%s" and filter "%s". Message was "%s"', [
                $hash ? sprintf('%s/%s', $hash, $path) : $path,
                $filter,
                $exception->getMessage(),
            ]), 0, $exception);
        }
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isWebpSupported(Request $request): bool
    {
        return false !== stripos($request->getHeader('accept') ?? '', 'image/webp');
    }
}
