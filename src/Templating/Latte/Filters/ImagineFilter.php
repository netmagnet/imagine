<?php

namespace Slts\Imagine\Templating\Latte\Filters;

use Nette\Utils\Strings;
use Slts\Glide\Parameters\ParameterFilter as GlideParameterFilter;
use Slts\Imagine\Bridge\Glide\FiltersConvertor;
use Slts\Imagine\Bridge\Glide\ParameterFilter;
use Slts\Imagine\Cache\CacheManager;

class ImagineFilter
{
    /**
     * @var CacheManager
     */
    private $cache;

    /**
     * @param CacheManager $cache
     */
    public function __construct(CacheManager $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Gets the browser path for the image and filter to apply.
     *
     * @param string      $path
     * @param string      $filter
     * @param array       $config
     * @param string|null $resolver
     * @param int         $referenceType
     *
     * @return string
     */
    public function __invoke($path, ...$parameters)
    {
        $resolver = null; // TODO not supported yet
        $count = \count($parameters);
        // imagine without filter
        if ($count === 1 && \is_array($parameters[0])) {
            $filter = null;
            $config = $parameters;
        }
        elseif ($count === 1 && \is_string($parameters[0]) && !Strings::contains($parameters[0], '=')) {
            $filter = $parameters[0];
            $config = [];
        }
        // imagine with filter
        elseif ($count === 2 && \is_string($parameters[0]) && \is_array($parameters[1])) {
            [$filter, $config] = $parameters;
        }
        // glide
        else {
            $parsedParams = $this->parseParameters($parameters);
            if (class_exists(GlideParameterFilter::class)) {
                $filteredParams = GlideParameterFilter::filter($parsedParams);
            } else {
                $filteredParams = ParameterFilter::filter($parsedParams);
            }

            [$filter, $config] = FiltersConvertor::glideToImagine($filteredParams);
        }

        $filter = $filter ?? 'default';

        return $this->cache->getBrowserPath(parse_url((string)$path, PHP_URL_PATH), $filter, $config, $resolver);
    }

    private function parseParameters(array $parameters)
    {
        $result = [];
        foreach ($parameters as $param) {
            [$key, $value] = explode('=', $param, 2);
            $result[$key] = $value;
        }

        return array_filter($result, function ($value){
            return null !== $value;
        });
    }
}
