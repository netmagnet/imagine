<?php

namespace Slts\Imagine\Cache\Resolver;

use Liip\ImagineBundle\Binary\BinaryInterface;
use Liip\ImagineBundle\Imagine\Cache\Helper\PathHelper;
use Liip\ImagineBundle\Imagine\Cache\Resolver\ResolverInterface;
use Nette\Http\Request;

class WebPathResolver implements ResolverInterface
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var string
     */
    protected $webRoot;

    /**
     * @var string
     */
    protected $cachePrefix;

    /**
     * @var string
     */
    protected $cacheRoot;

    /**
     * @param Request    $request
     * @param string     $webRootDir
     * @param string     $cachePrefix
     */
    public function __construct(
        Request $request,
        $webRootDir,
        $cachePrefix = 'media/cache'
    ) {
        $this->request = $request;

        $this->webRoot = rtrim(str_replace('//', '/', $webRootDir), '/');
        $this->cachePrefix = ltrim(str_replace('//', '/', $cachePrefix), '/');
        $this->cacheRoot = $this->webRoot.'/'.$this->cachePrefix;
    }

    /**
     * {@inheritdoc}
     */
    public function resolve($path, $filter)
    {
        return sprintf('%s/%s',
            rtrim($this->getBaseUrl(), '/'),
            ltrim($this->getFileUrl($path, $filter), '/')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function isStored($path, $filter)
    {
        return is_file($this->getFilePath($path, $filter));
    }

    /**
     * {@inheritdoc}
     */
    public function store(BinaryInterface $binary, $path, $filter)
    {
        \Nette\Utils\FileSystem::write(
            $this->getFilePath($path, $filter),
            $binary->getContent()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function remove(array $paths, array $filters)
    {
        if (empty($paths) && empty($filters)) {
            return;
        }

        if (empty($paths)) {
            $filtersCacheDir = [];
            foreach ($filters as $filter) {
                $filtersCacheDir[] = $this->cacheRoot.'/'.$filter;
            }

            \Nette\Utils\FileSystem::delete($filtersCacheDir);

            return;
        }

        foreach ($paths as $path) {
            foreach ($filters as $filter) {
                \Nette\Utils\FileSystem::delete($this->getFilePath($path, $filter));
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function getFilePath($path, $filter)
    {
        return $this->webRoot.'/'.$this->getFullPath($path, $filter);
    }

    /**
     * {@inheritdoc}
     */
    protected function getFileUrl($path, $filter)
    {
        return PathHelper::filePathToUrlPath($this->getFullPath($path, $filter));
    }

    /**
     * @return string
     */
    protected function getBaseUrl()
    {
        $port = '';
        //TODO
//        if ('https' === $this->request->getScheme() && 443 !== $this->request->getHttpsPort()) {
//            $port = ":{$this->request->getHttpsPort()}";
//        }
//
//        if ('http' === $this->request->getScheme() && 80 !== $this->request->getHttpPort()) {
//            $port = ":{$this->request->getHttpPort()}";
//        }

        $url = $this->request->getUrl();
        $baseUrl = $url->getBaseUrl();
        if ('.php' === mb_substr($url->getBaseUrl(), -4)) {
            $baseUrl = pathinfo($url->getBaseurl(), PATHINFO_DIRNAME);
        }
        $baseUrl = rtrim($baseUrl, '/\\');

        return sprintf('%s://%s%s%s',
            $url->getScheme(),
            $url->getHost(),
            $port,
            $baseUrl
        );
    }

    private function getFullPath($path, $filter)
    {
        // crude way of sanitizing URL scheme ("protocol") part
        $path = str_replace('://', '---', $path);

        return $this->cachePrefix.'/'.$filter.'/'.ltrim($path, '/');
    }
}
